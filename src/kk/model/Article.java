package kk.model;

/**
 * Created by wenceslaus on 11.03.17.
 */
public class Article {
    public static final boolean TYPE = false;

    public String title;
    public String text;
    public String imageURL;

    public void print() {
        System.out.println(title + " " + text);
    }
}
